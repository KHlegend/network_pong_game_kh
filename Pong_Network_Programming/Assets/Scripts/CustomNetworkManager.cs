﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Networking;

public class CustomNetworkManager : NetworkManager
{
    List<Player> players = new List<Player>();

    public Transform player1SpawnPoint;
    public Transform player2SpawnPoint;

    public GameObject ballPrefab;



    public override void OnServerConnect(NetworkConnection conn)
    {
        base.OnServerConnect(conn);

        //List of positions
        startPositions.Clear();
        Debug.Log("Player " + (numPlayers + 1) + " Connected");

        if (numPlayers == 0)
        {
            RegisterStartPosition(player1SpawnPoint);
        }

        if (numPlayers == 1)
        {
            RegisterStartPosition(player2SpawnPoint);
            //ball.SetActive(true);


            //players[1].RpcSpawnBall();
        }
    }

    //This happens when on the server when someone joins
    public override void OnServerAddPlayer(NetworkConnection conn, short playerID)
    {
        base.OnServerAddPlayer(conn, playerID);
        var tPlayer = conn.playerControllers.Select(cont => cont.gameObject.GetComponent<Player>()).FirstOrDefault();
        players.Add(tPlayer);


        if (numPlayers == 2)
        {
            //SpawnBall();
            GameObject ball = Instantiate(ballPrefab);
            ball.transform.position = Vector3.zero;
            NetworkServer.Spawn(ball);
            //ball.SetActive(true);
        }

    }


    public void DistributeUI(int p1, int p2)
    {
        foreach (Player player in players)
        {
            player.Rpc_UpdateUI(p1, p2);
        }
    }

    //public void SpawnBall()
    //{
    //    foreach (Player player in players)
    //    {
    //        player.RpcSpawnBall();
    //    }
    //}

    
}



