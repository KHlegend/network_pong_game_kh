﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class PlayerInputController : NetworkBehaviour
{
    //Script that handles input from two players
    //Player 1 => Controls left bat with W/S keys
    //Player 2 => Controls right bat with arrow keys

    public GameObject leftPaddle;
    public GameObject rightPaddle;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!hasAuthority) return;

        //Default speed of the bat to zero on every frame
        leftPaddle.GetComponent<Rigidbody>().velocity = new Vector3(0f, 0f, 0f);

        //Player 1 controls
        if (Input.GetKey(KeyCode.W))
        {
            //Move The paddle up by 1 velocity
            leftPaddle.GetComponent<Rigidbody>().velocity = new Vector3(0f, 7f, 0f);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            //Move the paddle down by 1 velocity
            leftPaddle.GetComponent<Rigidbody>().velocity = new Vector3(0f, -7f, 0f);
        }

        //If you aren't pressing either key, velocity will stay at zero

        //Default speed of the bat to zero on every frame
        rightPaddle.GetComponent<Rigidbody>().velocity = new Vector3(0f, 0f, 0f);


        //Player 2 controls
        if (Input.GetKey(KeyCode.UpArrow))
        {
            //Move The paddle up by 1 velocity
            rightPaddle.GetComponent<Rigidbody>().velocity = new Vector3(0f, 7f, 0f);
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            //Move the paddle down by 1 velocity
            rightPaddle.GetComponent<Rigidbody>().velocity = new Vector3(0f, -7f, 0f);
        }

        //If you aren't pressing either key, velocity will stay at zero
    }
}
