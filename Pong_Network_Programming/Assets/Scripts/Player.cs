﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class Player : NetworkBehaviour
{
    Rigidbody rb;
    [SerializeField] float moveSpeed = 7f;
    ScoreBoardController scoreboard;

    public GameObject ballPrefab;



    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        scoreboard = FindObjectOfType<ScoreBoardController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!hasAuthority) return;

        Vector3 v = Vector3.zero;
        if (Input.GetKey(KeyCode.W))
        {
            //Move The paddle up by 1 velocity
            v += Vector3.up * moveSpeed;


        }
        else if (Input.GetKey(KeyCode.S))
        {
            //Move the paddle down by 1 velocity
            v += Vector3.down * moveSpeed;
        }

        rb.velocity = v;



    }

    [ClientRpc]
    public void Rpc_UpdateUI(int p1, int p2)
    {
        scoreboard.playerOneScoreText.text = p1.ToString();
        scoreboard.playerTwoScoreText.text = p2.ToString();
    }

    //[ClientRpc]
    //public void RpcSpawnBall()
    //{
    //    GameObject ball = Instantiate(ballPrefab);
    //    ball.transform.position = Vector3.zero;
    //}
}
