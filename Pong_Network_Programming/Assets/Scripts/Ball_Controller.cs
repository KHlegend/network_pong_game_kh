﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Ball_Controller : NetworkBehaviour
{
    Rigidbody rb;
    float speedInXDirection = 0f;
    float speed;
    float originalSpeed = 8f;
    float speedBoost = 2f;


    // Start is called before the first frame update
    void Start()
    {
        speed = originalSpeed;
        rb = GetComponent<Rigidbody>();
        StartCoroutine(Pause());
    }

    // Update is called once per frame
    void Update()
    {
        //This indicates if the ball went to far to the left
        if(transform.position.x < -24)
        {
            transform.position = Vector3.zero;
            rb.velocity = Vector3.zero;

            ScoreBoardController.instance.GivePlayerTwoAPoint();

            StartCoroutine(Pause());
            speed = originalSpeed;
        }

        //This indicates if the ball went to far to the Right
        if (transform.position.x > 24)
        {
            transform.position = Vector3.zero;
            rb.velocity = Vector3.zero;

            ScoreBoardController.instance.GivePlayerOneAPoint();

            StartCoroutine(Pause());
            speed = originalSpeed;

        }
    }

    void LaunchBall()
    {
        transform.position = Vector3.zero;

        int xDirection = Random.Range(0, 2);

        int yDirection = Random.Range(0, 3);

        Vector3 launchDirection = new Vector3();

        if (xDirection == 0)
        {
            launchDirection.x = -8f;
        }
        if (xDirection == 1)
        {
            launchDirection.x = 8f;
        }

        if (yDirection == 0)
        {
            launchDirection.y = -8f;
        }
        if (yDirection == 1)
        {
            launchDirection.y = 8f;
        }

        rb.velocity = launchDirection;
    }

    IEnumerator Pause()
    {
        yield return new WaitForSeconds(2.5f);

        LaunchBall();
    }

    private void OnCollisionEnter(Collision hit)
    {
        speed += speedBoost;

        if (rb.velocity.magnitude < speed)
        {
            rb.velocity = rb.velocity.normalized * speed;
        }


        //if(hit.gameObject.name == "TopBounds")
        //{
        //    if(rb.velocity.x > 0f)
        //    {
        //        speedInXDirection = 8f;
        //    }

        //    if(rb.velocity.x < 0f)
        //    {
        //        speedInXDirection = -8f;
        //    }

        //    rb.velocity = new Vector3(speedInXDirection, -8f, 0f);
        //}

        //if (hit.gameObject.name == "BottomBounds")
        //{
        //    if (rb.velocity.x > 0f)
        //    {
        //        speedInXDirection = 8f;
        //    }

        //    if (rb.velocity.x < 0f)
        //    {
        //        speedInXDirection = -8f;
        //    }

        //    rb.velocity = new Vector3(speedInXDirection, 8f, 0f);
        //}

        //if(hit.gameObject.name == "LeftPaddle")
        //{
        //    rb.velocity = new Vector3(13f, 0f, 0f);

        //    //Check if we hit lower half of the bat
        //    if (transform.position.y - hit.gameObject.transform.position.y < -1)
        //    {
        //        rb.velocity = new Vector3(8f, -8f, 0f);
        //    }
        //    if (transform.position.y - hit.gameObject.transform.position.y > 1)
        //    {
        //        rb.velocity = new Vector3(8f, 8f, 0f);
        //    }
        //}

        //if (hit.gameObject.name == "RightPaddle")
        //{
        //    rb.velocity = new Vector3(-13f, 0f, 0f);

        //    //Check if we hit lower half of the bat
        //    if (transform.position.y - hit.gameObject.transform.position.y < -1)
        //    {
        //        rb.velocity = new Vector3(-8f, -8f, 0f);
        //    }
        //    if (transform.position.y - hit.gameObject.transform.position.y > 1)
        //    {
        //        rb.velocity = new Vector3(-8f, 8f, 0f);
        //    }
        //}
    }
}
