﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Networking;

public class ScoreBoardController : NetworkBehaviour
{
    public static ScoreBoardController instance;

    public Text playerOneScoreText;
    public Text playerTwoScoreText;

    public int playerOneScore;
    public int playerTwoScore;
    

    // Start is called before the first frame update
    void Start()
    {
        instance = this;

        playerOneScore = playerTwoScore = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GivePlayerOneAPoint()
    {
        playerOneScore += 1;

        playerOneScoreText.text = playerOneScore.ToString();
        CmdSendPlayer2UI();

    }

    public void GivePlayerTwoAPoint()
    {
        playerTwoScore += 1;

        playerTwoScoreText.text = playerTwoScore.ToString();
        CmdSendPlayer2UI();
    }

    [Command]
    void CmdSendPlayer2UI()
    {
        FindObjectOfType<CustomNetworkManager>().DistributeUI(playerOneScore, playerTwoScore);
    }

    







}
